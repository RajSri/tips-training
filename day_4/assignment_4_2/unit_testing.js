/* global describe:true, require: true, it:true */
var assert = require("assert");
var mymodule = require("./mymodule.js");

describe('app.js', function() {
describe('#isUndefined()', function(){
    it('should return true for an undefined variable', function(){
		var a, b = 5;
		assert.equal(true,  mymodule.isUndefined(a));
		assert.equal(false,  mymodule.isUndefined(b));
    });
  });
});

describe('app.js', function() {
describe('#isNull()', function(){
    it('should return true if an object is null', function(){
		var obj;
		var obj1 = 3;
		assert.equal(false,  mymodule.isNull(obj));
		assert.equal(false,  mymodule.isNull(obj1));
    });
  });
});

describe('app.js', function() {
describe('#isError()', function(){
    it('should return true if an object is null', function(){
		var err = new Error();
		var not_err = 4;
		assert.equal(true,  mymodule.isError(err));
		assert.equal(false,  mymodule.isError(not_err));
    });
  });
});

describe('app.js', function() {
describe('#isBoolean()', function(){
    it('should return true if an object is null', function(){
		var bool = new Boolean();
		var not_bool = "fdfd";
		assert.equal(true,  mymodule.isBoolean(bool));
		assert.equal(false,  mymodule.isBoolean(not_bool));
    });
  });
});