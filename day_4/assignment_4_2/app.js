/* global describe:true, console:true, require: true, it:true */
var mymodule = require('./mymodule.js');

var res,res1,res2,res3,res4,res5,res6,res7;

var a;
var b=5;

//isUndefined()
res= mymodule.isUndefined(a);
res1= mymodule.isUndefined(b);
console.log("\ntesting isUndefined() for undefined value" , res);
console.log("testing isUndefined() for defined value" , res1);

//isNull()
var obj = null;
res2= mymodule.isNull(obj);
console.log("\nTesting isNull() for null object" , res2);

//isNaN()
var nan_test = new String();
nan_test = "dfdf" ;
res3= mymodule.isNaN_test(nan_test);
console.log("\nPassisng a string in isNaN()",res3);

//isError()
var err = new Error();
var not_err = 6;
res4= mymodule.isError(err);
res5= mymodule.isError(not_err);
console.log("\nPassing an error as object in isError()",res4);
console.log("Passing a non error as object in isError()",res5);


//isBoolean()
var bool = new Boolean();
var not_bool = "gfg";
res6= mymodule.isBoolean(bool);
res7= mymodule.isBoolean(not_bool);
console.log("\nPassing a boolean as an object in isBoolean()",res6);
console.log("Passing a non boolean as an object in isBoolean()",res7);