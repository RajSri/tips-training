/* global describe:true, console:true, require: true, module:true */
function isUndefined(value) {
	if (typeof(value) ===  'undefined')
		return true;
	else 
		return false;
}

function isNull(value) {
	if (value ===  null)
		return true;
	else 
		return false;
}
	
function isNaN_test(value) {
	if (isNaN(value))
		return true;
	else 
		return false;
}

function isError(value) {
	if(value.constructor === Error) 
		return true;
	else 
		return false;
}


function isBoolean(value) {
	if(value.constructor === Boolean) 
		return true;
	else 
		return false;
}

//exporting modules
module.exports.isUndefined = isUndefined;
module.exports.isNull = isNull;
module.exports.isNaN_test = isNaN_test;
module.exports.isError = isError;
module.exports.isBoolean = isBoolean;