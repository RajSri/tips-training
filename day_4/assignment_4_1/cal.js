#! /usr/bin/env node
/* global console:true, require:true */

var Table = require('cli-table');
var arr = [];
var FebNumberOfDays ="";
var counter = 1;
var dateNow = new Date();
var month = dateNow.getMonth();
var nextMonth = month+1; //+1; //Used to match up the current month with the correct start date.
var prevMonth = month -1;
var day = dateNow.getDate();
var year = dateNow.getFullYear();


//Determing if February (28,or 29)  
if (month === 1){
	if ( (year%100 !== 0) && (year%4 === 0) || (year%400 === 0)){
		FebNumberOfDays = 29;
	}else{
		FebNumberOfDays = 28;
	}
}

// names of months and week days.
var monthNames = ["January","February","March","April","May","June","July","August","September","October","November", "December"];
var dayNames = ["Sunday","Monday","Tuesday","Wednesday","Thrusday","Friday", "Saturday"];
var dayPerMonth = ["31", ""+FebNumberOfDays+"","31","30","31","30","31","31","30","31","30","31"];


// days in previous month and next one , and day of week.
var nextDate = new Date(nextMonth +' 1 ,'+year);
var weekdays= nextDate.getDay();
var weekdays2 = weekdays;
var numOfDays = dayPerMonth[month];	
	 
 
// this leave a white space for days of pervious month.
while (weekdays>0){
	arr.push(' ');
   	weekdays--;
}
	
 // loop to build the calander body.
while (counter <= numOfDays){
 	arr.push(counter);
 	counter++;
}
var b_arr = [];
var i,j,temparray,chunk = 7;
for (i=0;i<arr.length; i+=chunk) {
	temparray= arr.slice(i,i+chunk);
	b_arr.push(temparray);
}
var table = new Table({
	chars: { 'top': '═' , 'top-mid': '╤' , 'top-left': '╔' , 'top-right': '╗',
			 'bottom': '═' , 'bottom-mid': '╧' , 'bottom-left': '╚' , 'bottom-right': '╝',
			'left': '║' , 'left-mid': '╟' , 'mid': '─' , 'mid-mid': '┼',
			'right': '║' , 'right-mid': '╢' , 'middle': '│' }
});
table.push ( ['','','',monthNames[month], year ,'',''],
			['Sun','Mon', 'Tues', 'wed','Thurs','Fri','Sat']
		   );
for(i=0; i<b_arr.length; i++ ) {
	table.push(b_arr[i]);
}

console.log(table.toString());
	
