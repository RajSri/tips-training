/* global console:true, describe:true, require: true, it:true, process:true */
var fs = require("fs");
var path = require("path");
var q = require("q");
var util = require('util')
    ;
function read_file() { // function to read the config file
	console.log("function called");
	var deferred = q.defer(); // creating promises
	fs.readFile('config.txt', 'utf8', function(err, data) { //fs.readFile for config.txt
		
		if (err) deferred.reject(err);
		
		else deferred.resolve(data);
	});
	return deferred.promise; //returning promise
}


var start, finish, text, f;
var texts = ["\nNew append"];
var promise = read_file(); 
promise.then(function(d) { // function on deferred.resolve
	console.log("\nResolved");
	var i,j,k;
	var splitted = d.split("\r\n");
	for (i=0; i < splitted.length; i++ ) {
		if(splitted[i] === 'destination') {
			console.log("target found");
			j=i;
			break;
		}
	}
	var sources = splitted.slice(1, j);
	var targets = splitted.slice(j+1, splitted.length);
	console.log(splitted);
	for (j=0; j<sources.length; j++) {
		(function(k) {
		start = process.hrtime();
		console.log("starting time" +start);		
		if (!fs.existsSync(targets[k])){ // checking if the target exists
			console.log("creating new folder"); // if not, creating the target folder
			fs.mkdir(targets[k], function() {
				f= targets[k] +'\\'+ k + ".txt";
				console.log(f);	
				fs.createReadStream(sources[k]).pipe(fs.createWriteStream(f)); // writing from source file to destination file
			});
		} else {
			console.log("Folder is already present");
			fs.createReadStream(sources[k]).pipe(fs.createWriteStream(targets[k] +'\\'+ k + ".txt"));
		}
			finish = process.hrtime(start); // calculating time taken in copying the file form start of the process
			console.log("Time from start to end of the file "+(k+1)+ " is " +finish);
			text = "\nTime taken for file "+(k+1)+" is " + finish[0] +" sec and "+finish[1] +" ns";
			texts.push(text);
			console.log("\n");
		}(j));
	}
	fs.appendFile('config.txt', texts, function (err) { // appending the tiem taken in the config file
		if (err) throw err;
		console.log('The "data to append" was appended to file!');
	});
} ,function()  { // function called when the defered.reject is returned
	console.log("\nRejected");
});

