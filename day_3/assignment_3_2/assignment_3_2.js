/* global console:true, describe:true, require: true, it:true, process:true */
var fs = require("fs");
var path = require("path");
var q = require("q");
var util = require('util')
    ;
function read_file() { // function to read the config file
	console.log("function called");
	var deferred = q.defer(); // creating promises
	fs.readFile('config.txt', 'utf8', function(err, data) { //fs.readFile for config.txt
		
		if (err) deferred.reject(err);
		
		else deferred.resolve(data);
	});
	return deferred.promise; //returning promise
}


var f;
var promise = read_file(); 
promise.then(function(d) { // function on deferred.resolve
	console.log("\nResolved");
	var i,j,k;
	var splitted = d.split("\r\n");
	for (i=0; i < splitted.length; i++ ) {
		if(splitted[i] === 'destination') {
			console.log("target found");
			j=i;
		}
	}
	var sources = splitted.slice(1, j);
	var targets = splitted.slice(j+1, splitted.length);
	for (j=0; j<sources.length; j++) {
		(function(k) {
		if (fs.existsSync(path.normalize(targets[k]))){// checking if the target exists
			console.log("Folder is already present");
			fs.createReadStream(path.normalize(sources[k])).pipe(fs.createWriteStream(path.normalize(f)));
		} else {
			console.log("creating new folder"); // if not, creating the target folder
			folderNorm = path.normalize(targets[k]); // if not, creating the target folder
			fs.mkdir(folderNorm, function() {
				f= targets[k] +'\\'+ k + ".txt";
fs.createReadStream(path.normalize(sources[k])).pipe(fs.createWriteStream(path.normalize(f))); // writing from source file to destination file
			});
		}
		}(j));
	}
} ,function()  { // function called when the defered.reject is returned
	console.log("\nRejected");
});


