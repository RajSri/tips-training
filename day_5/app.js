/*global console:true, require:true */
var fs = require("fs");
var path = require("path");
var q = require("q");
var http = require("http");


var p = "http://nodejs.org/dist/v0.12.4/node.exe";
var size_sum =0;
function read() {
	console.log("function called");
	var deferred = q.defer();
http.createServer(p, function (err, files) {	
	if (err) {
		deferred.reject(err);
	} else {
		deferred.resolve(files);
	}

});
	return deferred.promise;
}


var promise = read();
promise.then(function(files) {
	console.log("resolved");
},function()  {
	console.log("failure");
});
	