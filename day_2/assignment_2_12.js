
/* global console:true, require:true*/
var prompt = require('prompt');
Number.prototype.fibonacci = function(n) {
	var a = 0;
	var b = 1;
	var count = 0;
	this.limit = n;
	 var c;
	while(count<this.limit) {
		count++;
		c= a+b;
		a=b;
		b=c;
	}
	this.result = c;
	return this.result;
};

String.prototype.removeVowels = function(str) {
	this.result = str.replace(/[aeiou]/gi, '');
	return this.result;
};

prompt.start();




console.log("Do you want to work on number or string");
console.log("1. NUMBER 2. STRING");
prompt.get(['option'], function(err, result) { //getting input from user
	var selected_choice = parseInt(result.option);
	switch (selected_choice) {
		case 1: number_man();
			break;
		case 2: string_man();
			break;
		default : default_message();
			break;
	}
});

var myNum = new Number();
var myStr = new String();

function number_man () {
prompt.get(['number'],function(err,result) {
	var num = parseInt(result.number);
	myNum.fibonacci(num);
	console.log(myNum);
});
}

function string_man() {
prompt.get(['string'], function(err, result) {
	var str = result.string;
	myStr.removeVowels(str);
	console.log(myStr);
});
}


function default_message() { // function to display default message
	console.log("Please enter a valid choice");
}