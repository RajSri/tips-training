//program to demonstrate constructor invocation using arithmetic operations
/*global console:true, require:true*/
//creating a common constructor for the objects
var assign = function (a,b) { //constructor
	this.number_1 = a;
	this.number_2 = b;
};
 
assign.prototype.add = function() {
	console.log('Sum:' +( this.number_1 + this.number_2));
};
assign.prototype.minus = function() {
	console.log('Difference:' +( this.number_1 - this.number_2));
};
assign.prototype.multiply = function() {
	console.log('Product:' +(this.number_1 * this.number_2));
};
assign.prototype.divide = function() {
	console.log('Division:' +( this.number_1 / this.number_2));
};

var set1 = new assign(3,4); //Constructor invocation
var set2 = new assign(10,20); //Constructor invocation

console.log("\nFor 1st set of numbers:");
set1.add();
set1.minus();
set1.multiply();
set1.divide();

console.log("\nFor 2nd set of numbers:");
set2.add();
set2.minus();
set2.multiply();
set2.divide();
