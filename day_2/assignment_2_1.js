/* global console:true, module:true*/
var camera = [ 
	{name:"Nikon", pixels:1},
	{name:"Soliton", pixels:3},
	{name:"Sony", pixels:2}
];

var display = function(camera) {
	console.log("Displaying name in for loop");
	var i;
	var arr= [];
	for (i in camera) {
		console.log(camera[i].name);
		arr.push(camera[i].name);
	}
	return arr;
};

display(camera);

//exporting module
module.exports.dis = display;
