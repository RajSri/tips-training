//Program to reverse a given sentence
/*global console:true, require:true*/

reverse_sentence("This is TIPS 2015"); //function invocation

function reverse_sentence(input) { //function calleds
	var out_sentence = reverser(input);
	function reverser(inputgot) { //function to reverse the sentence
		var output = new Array();
		output.push(inputgot.split("").reverse().join(""));
		return output.join("");
	}
	console.log("\nFinal output: ");
	console.log(out_sentence);
}

