//program to enumerate a list of interested things
/*global console:true, require:true*/
var interested_things = {
	Person : "Mom",
	Color : "blue",
	Author : "Enid Blyton",
	Novel : "Gulliver's Travels"
	};


console.log("Displaying Array");
console.log(interested_things);
console.log("\nEnumerating the interested_things:");
// Printing property names and values using Array.forEach
Object.getOwnPropertyNames(interested_things).forEach(function(val, idx, array) {
  console.log(val + ' -> ' + interested_things[val]);
});
