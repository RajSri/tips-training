//Program to reverse a given sentence
/*global console:true, require:true*/
var prompt = require('prompt'); //requiring prompt module
prompt.start(); //starting prompt

console.log("Enter your choice to work in array");
console.log("1. view stack 2. PUSH 3.POP ");
prompt.get(['res'], function (err, result) { //getting input through prompt
	var selected_option = parseInt(result.res);
	console.log(selected_option);
	switch (selected_option) { // switching between choices
		case 1 :
			view();
			break;
		case 2: push_item();
			break;
		case 3: pop_item();
			break;
		default: 
			default_message();
			break;
	}
});


var stack = [ //array declaration
	{ firstName : "Allen",
	  lastName : "Maxwell"
	},
	{
		firstName : "Alex",
		lastName : "Killer"
	}
	];

function view() { // funtion to view stack
	console.log(stack);
}

function push_item() { //function to push into stack
	prompt.get(['firstName'], function(err,result) {
		var entered_firstName = result.firstName;
		var stack1 = {};
		stack1.firstName = entered_firstName;
		prompt.get(['lastName'], function(err, result) {
			var entered_lastName = result.lastName;
			stack1.lastName = entered_lastName;
			stack.push(stack1);
			console.log(stack);
		});
	});
}


function pop_item() { //function to pop an element from stack
	console.log("Before pop operation");
	console.log(stack);
	stack.pop();
	console.log("After pop operation");
	console.log(stack);
	
}

function default_message() { // function to display default message
	console.log("Please enter a valid choice");
}