//program to demonstrate apply invocation using arithmetic operations
/*global console:true, require:true*/
var add = function() {
        return (this.num1 + this.num2);
};
var sub = function() {
        return (this.num1-this.num2);
};
var product = function() {
        return (this.num1*this.num2);
};
var divide = function() {
        return (this.num1/this.num2);
};


var array = {
	num1 : 10,
	num2 : 20
};

var sum = add.apply(array); //apply invocation
console.log('Sum :' +sum);

var minus = sub.apply(array); //apply invocation
console.log('Difference :' +minus); 

var multiply = product.apply(array); //apply invocation
console.log('Product:' +multiply);

var division = divide.apply(array); //apply invocation
console.log('Division:' +division);

