/*global console:true*/
var camera =
	{
		name:"Nikon",
		pixels:18,
		speed : "4.3 frames per second",
		screen_size : "3 inches"
	};

var display = function() {
console.log("\nDisplaying in for loop");
	var i;
	for ( i in camera) {
		console.log(camera[i]);
	}
};

display();


console.log("\nEnumerating the object:");
// Printing property names and values using Array.forEach
Object.getOwnPropertyNames(camera).forEach(function(val, idx, array) {
  console.log(val + ' -> ' + camera[val]);
});
