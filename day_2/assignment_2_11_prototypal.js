/*global console:true, require:true, module:true */
/*Explaining thes type of inheritance followed in the program


Base function :            		SHAPE, LINE
								 |	\/  |
Derived function:    		  Square,Rectangle
								|
Sub Derived function:			Rhombus


*/

//creating Base function "Shape"
var prompt = require ('prompt'); //requiring prompt module
prompt.start(); // starting prompt

var shape = {
	myshape : function(name) {
		console.log("Creating a shape");
		this.name = name;
		return this.name;
	}
};


var line = {
	myline : function(a) {
		console.log("Dimension 1 created");
		this.dimension1 = a;
		return this.dimension1;
	
	}
};

 
//Creating derived fuction "Square"
 var square = {
	 square_fun : function(name) {
		 console.log("Creating a square");
		 this.name = name;
	 },
		 
};
square = Object.create(shape);//square inherits properties of shape
//creating an object for square
var mysquare = Object.create(square);


 //Creating derived fuction "Rectangle"
var myrectangle = {
	rec_fun : function() {
		this.name = "rec";
		console.log("Creating a rectangle");
	}		
};



prompt.get(['dim1','dim2'], function(err,result) {

	var num1 = parseInt(result.dim1);
	var num2 = parseInt(result.dim2);
	try {
		if(num1 === num2) {
			square_obj(num1,num2);
		} else {
			if(num2 !== null) {
				rectangle_obj(num1,num2);
			} else {
				throw "Sides of square must be equal";
			}
		} 
	}catch (e) {
		console.log(e);
	}
});

function square_obj(num1,num2) {
	console.log("Dimensions are equal, Forming SQUARE");
	mysquare = Object.create(line);// object of square inherits properties of line
	mysquare.myline(num1);	
	mysquare.mydimensions = function() {
		console.log("Dimension2 created");
		this.dimension2 = num2;
		this.area = this.dimension2*num1;
		return this.area;
	};
	mysquare.mydimensions();
	console.log(mysquare);
	console.log("\n");
	
	//Creating derived function of square "Rhombus"
	var rhombus = {
		rhom_fun : function(name) {
			this.name= name;
			console.log("Creating a rhombus");
		}
	};
	
	// creating object for rhombus
	var myrhombus = Object.create(square);//rhombus inherits properties of square

	myrhombus.angle = function() {
		console.log("Rhombus is inherited form square");
		console.log("Angle is acute for rhombus");
		this.angle = 80;
	};
	myrhombus.angle();
	console.log(myrhombus);
	return mysquare.area;
}

function rectangle_obj(num1,num2) {
	console.log("Dimensions are not equal, Forming RECTANGLE");
	
	myrectangle = Object.create(line);// object of rectangle inherits properties of line
	myrectangle.myline(num1);
	myrectangle.mydimensions = function() {
		console.log("Dimension2 created");
		this.dimension2 = num2;
		this.area = this.dimension2*num1;
		return this.area;
	};
	myrectangle.mydimensions();
	console.log(myrectangle);
	console.log("\n");
	return myrectangle.area;
}

//exporting module
module.exports.square_obj = square_obj;
module.exports.rectangle_obj = rectangle_obj;
	
	





 




