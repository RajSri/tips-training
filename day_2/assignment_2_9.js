//program to demonstrate arithmetic operations using closures
/*global console:true, require:true*/
var prompt = require ('prompt'); //requiring prompt module
prompt.start(); // starting prompt
var add = function(num1, num2) {
       function add_closure() {
			return (num1 + num2);
		}
	return add_closure();
	};
var sub = function(num1, num2) {
	function sub_closure() {
        return (num1-num2);
	}
	return sub_closure();
};
var product = function(num1, num2) {
	function mul_closure() {
        return (num1*num2);
	}
	return mul_closure();
};
var divide = function(num1, num2) {
	function div_closure() {
        return (num1/num2);
	}
	return div_closure();
};

prompt.get(['num1' , 'num2'], function(err,result) {
	var num1 = parseInt(result.num1);
	var num2 = parseInt(result.num2);
	var sum = add(num1,num2);
	console.log('Sum :' +sum);
	var minus = sub(num1,num2);
	console.log('Difference :' +minus); 
	var multiply = product(num1,num2);
	console.log('Product:' +multiply);
	var division = divide(num1,num2);
	console.log('Division:' +division);
});

