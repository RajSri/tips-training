//program to demonstrate function invocation using arithmetic operations
/*global console:true, require:true*/
var prompt = require ('prompt'); //requiring prompt module
prompt.start(); // starting prompt
var result = 100; //global variable
var obj = new Object();
obj.arithmetic= {
	result : 0,
	add: function(a,b) {
        this.result = a+b ;
		var assign = this;
		 var sum = function() {
		console.log('Sum is :' + assign.result);
			 return assign.result;
		 };
		 sum(); //Function invocation
	},
	sub: function(a, b) {
        this.result = a - b;
		var assign = this;
		var difference = function() {
		console.log('Difference is :' + assign.result);
			return assign.result;
		};
		difference(); //Function invocation
	},
	multiply: function(a , b) {
		this.result = a * b;
		var assign = this;
		var product = function() {
			console.log('Product is :' + assign.result);
			return assign.result;
		};
		product();  //Function invocation
	},
	div: function(a,b) {
        this.result = a / b;
		var assign = this;
		var division = function() {
		console.log('Divison result is :' + assign.result);
			return assign.result;
		};
		division(); //Function invocation
    }
};

//invoking methods
prompt.get(['num1' , 'num2'], function(err,result) {
	var num1 = parseInt(result.num1);
	var num2 = parseInt(result.num2);
	obj.add(num1,num2);
	obj.sub(num1,num2);
	obj.multiply(num1,num2);
	obj.div(num1,num2);
});

//
////exporting module
//module.exports.sum = obj.arithmetic.sum;
//module.exports.difference = obj.arithmetic.sub;
//module.exports.product = obj.arithmetic.multiply;
//module.exports.division = obj.arithmetic.div;