/*global console:true, require:true, module:true */
/*Explaining thes type of inheritance followed in the program


Base function :            		SHAPE, LINE
								 |	\/  |
Derived function:    		  Square,Rectangle
								|
Sub Derived function:			Rhombus


*/

//creating Base function "Shape"
var prompt = require ('prompt'); //requiring prompt module
prompt.start(); // starting prompt

var shape = function(name) {
	console.log("Creating a shape");
	this.name = name ;
};


var line = function(a) {
	console.log("Dimension 1 created");
	 this.dimension1 = a;
};

 
//Creating derived fuction "Square"
 var square = function(name) {
	 console.log("Creating a square");
	this.name = name;
};
square.prototype = new shape();//square inherits properties of shape
square.prototype.dimensions = function(line2) {
	console.log("Dimension2 created");
	this.dimension2 = line2;
	this.area = this.dimension2*mysquare.prototype.dimension1;
};

 //Creating derived fuction "Rectangle"
var rectangle = function(name) {
	this.name = name;
	 console.log("Creating a rectangle");
};
rectangle.prototype = new shape();//rectangle inherits properties of shape
rectangle.prototype.dimensions = function(line2) {
	console.log("Dimension2 created");
	this.dimension2 = line2;
	this.area = this.dimension2*myrectangle.prototype.dimension1;
};

//creating an object for square
var mysquare = new square("square");
//creating an object for rectangle
var myrectangle = new rectangle("Rectangle");

prompt.get(['dim1','dim2'], function(err,result) {

	var num1 = parseInt(result.dim1);
	var num2 = parseInt(result.dim2);
	try {
		if(num1 === num2) {
			square_obj(num1,num2);
		} else {
			if(num2 !== null) {
				rectangle_obj(num1,num2);
			} else {
				throw "Sides of square must be equal";
			}
		} 
	}catch (e) {
		console.log(e);
	}
});

function square_obj(num1,num2) {
	console.log("Dimensions are equal, Forming SQUARE");
	mysquare.prototype = new line(num1);// object of square inherits properties of line
	mysquare.dimensions(num2);
	console.log(mysquare);
	console.log("\n");
	//Creating derived function of square "Rhombus"
	var rhombus = function(name) {
		this.name= name;
		console.log("Creating a rhombus");
	};
	rhombus.prototype = new square();//rhombus inherits properties of square
	rhombus.prototype.angle = function() {
		this.angle = 80;
	};
	//creating a object for rhombus
	var myrhombus = new rhombus("Rhombus1");
	myrhombus.dimensions(num2);
	myrhombus.angle();
	console.log(myrhombus);
	return mysquare.area;
}

function rectangle_obj(num1,num2) {
	console.log("Dimensions are not equal, Forming RECTANGLE");
	myrectangle.prototype = new line(num1);// object of rectangle inherits properties of line
	myrectangle.dimensions(num2);
	console.log(myrectangle);
	console.log("\n");
	return myrectangle.area;
}

//exporting module
module.exports.square_obj = square_obj;
module.exports.rectangle_obj = rectangle_obj;
	
	





 




