/*global console:true*/
var camera =
	{
		name:"Nikon",
		pixels:18,
		speed : "4.3 frames per second",
		screen_size : "3 inches"
	};

var display = function() {
	console.log("Displaying in for loop");
	var i;
	for ( i in camera) {
		if(typeof(camera[i]) === 'number')
			console.log(camera[i]);
	}
};

display();

