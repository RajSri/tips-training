//program to demonstrate the difference between splice and slice method
/*global console:true, require:true*/
var prompt = require('prompt'); //requiring prompt module
prompt.start(); // starting prompt
var my_arr = ['a','b','c','d','e'];
console.log(my_arr);


console.log("Do you want to splice or slice the array");
console.log("1. SPLICE 2. SLICE");
prompt.get(['option'], function(err, result) { //getting input from user
	var selected_choice = parseInt(result.option);
	switch (selected_choice) {
		case 1: splice_arr();
			break;
		case 2: slice_arr();
			break;
		default : default_message();
			break;
	}
});


function splice_arr() { 
	my_arr.splice(2, 1, "new"); //splices the array 
	console.log(my_arr);
}

function slice_arr() {
	var new_arr = my_arr.slice(2,4); //returns selected elements in an array
	console.log(new_arr);
}


function default_message() { // function to display default message
	console.log("Please enter a valid choice");
}