//program to demonstrate method invocation using arithmetic operations
/*global console:true, require:true*/
var prompt = require ('prompt'); //requiring prompt module
prompt.start(); // starting prompt
var obj = new Object();
obj.arithmetic = {
	number_1 : 4,
	number_2 : 4,
	result : 0,
    add: function(n1, n2) {
		this.number_1 = n1;
		this.number_2 = n2;
        this.result = this.number_1 + this.number_2;
		console.log('Sum is :' + this.result);
		return this.result;
	},
	sub: function(n1, n2) {
		this.number_1 = n1;
		this.number_2 = n2;
        this.result = this.number_1 - this.number_2;
		console.log('Difference is :' + this.result);
		return this.result;
	},
	multiply:function(n1, n2) {
		this.number_1 = n1;
		this.number_2 = n2;
		this.result = this.number_1 * this.number_2;
		console.log('Product is :' + this.result);
		return this.result;
	},
	div: function(n1, n2) {
		this.number_1 = n1;
		this.number_2 = n2;
        this.result = this.number_1 / this.number_2;
		console.log('Divison result is :' + this.result);
		var res = Math.ceil(this.result);
		return res;
    }
};


prompt.get(['num1' , 'num2'], function(err,result) {
	var num1 = parseInt(result.num1);
	var num2 = parseInt(result.num2);
	obj.add(num1,num2);  //Method invocation for add
	obj.sub(num1,num2); //Method invocation for sub
	obj.multiply(num1,num2); //Method invocation for multiply
	obj.div(num1,num2); //Method invocation for  for div
});


//exporting module
module.exports.add = obj.arithmetic.add;
module.exports.sub = obj.arithmetic.sub;
module.exports.mul = obj.arithmetic.multiply;
module.exports.div = obj.arithmetic.div;