/* global describe:true, require: true, it:true */
var assert = require("assert");
var assignment_2_1 = require("./assignment_2_1.js");
var assignment_2_5 = require("./assignment_2_5.js");
var assignment_2_6 = require("./assignment_2_6.js");
var pseudo = require("./assignment_2_11_pseudo.js");
var prototypal = require("./assignment_2_11_prototypal.js");

describe('assignment_2_1.js', function() {
describe('#display()', function(){
	var obj =[ 
		{name:"Soliton", pixels:3},
		{name:"Sony", pixels:2}
	];
	it('should display the name of the camera object', function(){
      assert.deepEqual(["Soliton", "Sony"], assignment_2_1.dis(obj));
    });
  });
});


describe('assignment_2_5.js', function() {
describe('Method invocation', function(){
	it('Arithmetic operations using method invocation', function(){
      assert.equal(5, assignment_2_5.add(2,3));
      assert.equal(-1, assignment_2_5.sub(2,3));
      assert.equal(6, assignment_2_5.mul(2,3));
      assert.equal(1, assignment_2_5.div(2,3));
    });
  });
});

describe('assignment_2_11_pseudo.js', function() {
describe('Checking Psuedoclassical inheritance', function(){
	it('Invoking function to create rectangle and square objects', function(){
      assert.equal(800, pseudo.rectangle_obj(20,40));
      assert.equal(400, pseudo.square_obj(20,20));
    });
  });
});


describe('assignment_2_11_protypal.js', function() {
describe('Checking Prototypal inheritance', function(){
	it('Invoking function to create rectangle and square objects', function(){
      assert.equal(800, prototypal.rectangle_obj(20,40));
      assert.equal(400, prototypal.square_obj(20,20));
    });
  });
});






//describe('assignment_2_6.js', function() {
//describe('Method invocation', function(){
//	it('Arithmetic operations using method invocation', function(){
//      assert.equal(5, assignment_2_6.add(2,3));
//      assert.equal(-1, assignment_2_6.sub(2,3));
//      assert.equal(6, assignment_2_6.multiply(2,3));
//      assert.equal(0.5, assignment_2_6.div(2,3));
//    });
//  });
//});



