/* global describe:true, require: true, it:true */
var assert = require("assert");
var assignment_1 = require("./assignment_1.js");

describe('assignment_1.js', function() {
describe('#fibonacci()', function(){
    it('should return [0,1,1,2,3] when properly generated', function(){
      assert.deepEqual([0,1,1,2,3], assignment_1.fib_func(5));
    });
  });
});


describe('assignment_1.js', function() {
describe('#reverse_sentence()', function(){
    it('should return rsjr when worked properly', function(){
      assert.equal("rsjr", assignment_1.rev_func("raj sree"));
    });
  });
});


describe('assignment_1.js', function() {
describe('#sorting_students()', function(){
		var students_to_test = [
		{ID:3, firstName:"Prema", lastName:"Latha", age:15},
		{ID:2, firstName:"Raj", lastName:"Ganesh", age:13}
	];
    it('should be sorted in ascending/descending order of ID', function(){
      assert.deepEqual([ 3, 2 ], assignment_1.desc_sort(students_to_test));
      assert.deepEqual([ 2, 3 ], assignment_1.asec_sort(students_to_test));
    });
  });
});



describe('assignment_1.js', function() {
describe('#maintaing_books()', function(){
			var books_to_test = [
		{author: "author1", book: "book1"},
		{author: "author2", book: "book2"},
				
	];
    it('should be search and add a new book to sample books to test', function(){
     assert.deepEqual({author: "author2", book: "book2"}, assignment_1.search1(books_to_test, "book2" ));
     assert.deepEqual([{author: "author1", book:"book1"},{author: "author2", book:"book2"}, {author: "author3", book:"book3"}], assignment_1.add(books_to_test, "author3", "book3"));
    });
  });
});