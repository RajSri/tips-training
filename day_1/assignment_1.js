/* global console:true, require:true, process:true, module:true*/
var prompt = require('prompt'); //requiring prompt module
prompt.start(); //starting prompt
main();
function main() {
console.log("Select the operation to be performed\n");
console.log("1: Forming diamond pattern 2: Generating Fibonacci series\n");
console.log("3: Sorting students list 4: Removing spaces and vowels form the sentences\n");
console.log("5: Maintaining books\n");

prompt.get(['option'], function (err,result) { // getting option from user to perform function
	var function_selected = parseInt(result.option);
	switch (function_selected) {
		case 1 : pattern();
			break;
		case 2 : fibonacci();
			break;
		case 3: sort_students();
			break;
		case 4: reverse_sentence();
			break;
		case 5: maintaing_books();
			break;
		default : out_of_choice();
			break;

	}
});
}


function pattern() { //function to form diamond pattern 
	console.log("Enter lines required for pattern");
	prompt.get(['lines'], function (err, result) { //getting number of lines to form pattern
		if (result.lines !== null) {
			console.log( ' lines: ' +result.lines);
			if ( result.lines%2 === 0) {
				console.log("Please enter a odd number of lines to form a perfect pattern");
			} else {
				var middle = Math.ceil(result.lines/2);
				var space = middle - 1;
				for( i=1; i<= middle; i++) {
					for (j=1; j<= space ; j++) {
						process.stdout.write(" ");	
					}
					space--;
					for (j=1; j<= 2*i-1 ; j++) {
						process.stdout.write("*");
					}
					process.stdout.write("\n");
				}
				space = 1;
				for( i=1; i<= middle - 1; i++) {
					for (j=1; j<= space ; j++) {
						process.stdout.write(" ");					
					}
					space++;
					for (j=1; j< 2*(middle-i) ; j++) {
						process.stdout.write("*");
					}	
					process.stdout.write("\n");
				}
			}
		}	
	});
}


function fibonacci() { //function to generate fibonacci series
	console.log("Enter number");
	prompt.get(['number'], function (err, result) {
		if (result.number !== null) {
			var selected_number = parseInt(result.number);
			fib_func(selected_number);
		}
	});
}

function fib_func(num) {
	var a=0;
	var b=1;
	var ctest = 2;
	var c;
	var count = 2;
	var arr = [];
	console.log(a); 
	console.log(b);
	arr.push(a);
	arr.push(b);
	while (count < num) {
		count++;
		c=a+b;
		console.log(c);
		arr.push(c);
		a=b;
		b=c;
	}
	return arr;
}



function reverse_sentence() { //function to reverse the sentence without vowels and spaces	
	console.log("Enter a sentence: ");
	prompt.get(['sentence'], function (err, result) {
		var input = result.sentence;
		rev_func(input);
	});
}

function rev_func(input) {
		var input_withnovowels = removeVowels(input);
		console.log("\nAfter removing vowels:");
		console.log(input_withnovowels);
		var input_withnospaces = removeSpaces(input_withnovowels);
		console.log("\nAfter removing spaces also:");
		console.log(input_withnospaces);
		var out_sentence = reverser(input_withnospaces);
		function removeVowels(str) { //function to remove vowels
			return str.replace(/[aeiou]/gi, '');
		}
		function removeSpaces(str) { //function to remove spaces
			return str.replace(/\s+/g, '');
		}
		function reverser(inputgot) { //function to reverse the sentence
			var output = new Array();
			output.push(inputgot.split("").reverse().join(""));
			return output.join("");
		}
		console.log("\nFinal output: ");
		console.log(out_sentence);
	return out_sentence;
}


function sort_students() { //function to sort students based on id
	console.log("Students list");
	var students = [
		{ID:1, firstName:"Raj", lastName:"Sree", age:14},
		{ID:3, firstName:"Prema", lastName:"Latha", age:15},
		{ID:2, firstName:"Raj", lastName:"Ganesh", age:13}
	];
	console.log("Before sorting");
	console.log(students);
	console.log('Enter 1. For ascending sort using ID 2. For descending sort using ID');
	prompt.get(['sort_order'], function (err, result) { //sorting in ascending order
		var choice = parseInt(result.sort_order);
		if(choice === 1) {
			asec_sort(students);
		} else {
			if (choice === 2) {
				desc_sort(students);
			} else {
				console.log("Select either ascending or descending sort order");
			}
		}
	});
}

function asec_sort(students) { // sorting in ascending order
	students.sort(function (a, b) {
		if (a.ID > b.ID) {
			return 1;
		}
		if (a.ID < b.ID) {
			return -1;
		}
		// a must be equal to b
		return 0;
	});
	console.log("After ascending sort");
	console.log(students);
	var sorted_arr = [];
	for ( i=0 ; i< students.length ; i++) {
		sorted_arr.push(students[i].ID);
	}
	return sorted_arr;
} 

function desc_sort(students) { //sorting  in descending order
	students.sort(function (a, b) {
		if (a.ID < b.ID) {
			return 1;
		}
		if (a.ID > b.ID) {
			return -1;
		}
		// a must be equal to b
		return 0;
	});
	console.log("After descending sort");
	console.log(students);
	var sorted_arr = [];
	for ( i=0 ; i< students.length ; i++) {
		sorted_arr.push(students[i].ID);
	}
	return sorted_arr;
} 

function maintaing_books() { //function to maintain books
	console.log("Book list");
	var books = [
		{author:"Jonathan Swift", book:"Gulliver's Island"},
		{author:"Charles Dickens", book:"David Copperfield"},
		{author:"George Grossmith", book:"The Diary of a Nobody "}
	];
	console.log(books);
	console.log("Enter your choices 1. Add new book and author 2. Search for book");
	prompt.get(['choice_entered'], function (err, result) {
		var choice = parseInt(result.choice_entered);
		if ( choice === 1 ) { // to add book
			console.log("Enter the author name");
			prompt.get(['author'],function(err,result1) {
				console.log("Enter the book name");
				prompt.get(['book'],function(err,result2) {
					add(books, result1.author, result2.book);
				});
			});
		} else {
			if ( choice === 2) { //to search for a book
				console.log("Enter the book to be searched");
				prompt.get(['book'],function(err,result3) {
					var value_entered = result3.book;
					try{
						if (value_entered.trim().length === 0 ) {
							throw "Exception: Enter a value"; //handling exception
						}
					} catch (err) { //catching exception
						console.log(err);
					}
				search1(books, value_entered);
			});
			} else {
				console.log("Select either of the above choices");
			}
		}
	});
}

function add(books, author_given , book_given) { //function to add a book to book store
	books.push({author: author_given , book: book_given });
	console.log("After adding:");
	console.log(books);
	return books;
}
		
function search1(books , value_entered) { //function to search for a book in book store
	var result = search(value_entered, books);
	if (typeof(result) === 'undefined') {
		console.log( "The book is not present"); //The book not present
	} else {
		console.log("Sarched detail:");
		console.log(result);
	}
	function search(key, myArray){ //function to search for a book
		for (var i=0; i < myArray.length; i++) {
			if (myArray[i].book === key) {
				return myArray[i];
			}
		}
	}
	return result;
}
			




function out_of_choice () { //function called by default choices
	console.log("Please select an valid choice as stated");
}
	
//exporting modules
module.exports.fib_func = fib_func;
module.exports.rev_func = rev_func;
module.exports.asec_sort = asec_sort;
module.exports.desc_sort = desc_sort;
module.exports.add = add;
module.exports.search1 = search1;